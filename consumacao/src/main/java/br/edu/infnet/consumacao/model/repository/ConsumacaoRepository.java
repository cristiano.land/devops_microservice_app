package br.edu.infnet.consumacao.model.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.edu.infnet.consumacao.model.entities.Consumacao;

@Repository
public interface ConsumacaoRepository extends CrudRepository<Consumacao, Long>{

}

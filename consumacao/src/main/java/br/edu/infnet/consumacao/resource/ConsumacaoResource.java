package br.edu.infnet.consumacao.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.edu.infnet.consumacao.model.entities.Consumacao;
import br.edu.infnet.consumacao.model.services.ConsumacaoService;

@RestController
@RequestMapping("/consumacoes")
public class ConsumacaoResource {
	
	@Autowired
	private ConsumacaoService consumacaoService; 
	
	@GetMapping
	public List<Consumacao> getConsumacoes(){
		return consumacaoService.getAll();
		
	}
}
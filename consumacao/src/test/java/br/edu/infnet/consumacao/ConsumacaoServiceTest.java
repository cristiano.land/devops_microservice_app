package br.edu.infnet.consumacao;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import br.edu.infnet.consumacao.model.entities.Consumacao;
import br.edu.infnet.consumacao.model.repository.ConsumacaoRepository;
import br.edu.infnet.consumacao.model.services.ConsumacaoService;

@SpringBootTest
public class ConsumacaoServiceTest {

    @Autowired
    private ConsumacaoService service;

    @MockBean
    private ConsumacaoRepository repository;

    @Test
    public void testGetAll() {
        // given
        Consumacao c1 = new Consumacao(1, 1, 10, "Consumação 1");
        Consumacao c2 = new Consumacao(2, 20, 20, "Consumação 2");
        List<Consumacao> expectedConsumacoes = Arrays.asList(c1, c2);
        Mockito.when(repository.findAll()).thenReturn(expectedConsumacoes);

        // when
        List<Consumacao> actualConsumacoes = service.getAll();

        // then
        assertThat(actualConsumacoes).isEqualTo(expectedConsumacoes);
        Mockito.verify(repository, Mockito.times(1)).findAll();
    }

}

package br.edu.infnet.order.Resource.dto;

import java.util.List;

public class CheckinResponseDTO {
	private Integer checkinId;
	private ClientDTO clientDTO;
	private List<ConsumacaoDTO> consumacoes;
	public CheckinResponseDTO(Integer checkinId, ClientDTO clientDTO, List<ConsumacaoDTO> consumacoes) {
		super();
		this.checkinId = checkinId;
		this.clientDTO = clientDTO;
		this.consumacoes = consumacoes;
	}
	public Integer getCheckinId() {
		return checkinId;
	}
	public void setCheckinId(Integer checkinId) {
		this.checkinId = checkinId;
	}
	public ClientDTO getClientDTO() {
		return clientDTO;
	}
	public void setClientDTO(ClientDTO clientDTO) {
		this.clientDTO = clientDTO;
	}
	public List<ConsumacaoDTO> getConsumacoes() {
		return consumacoes;
	}
	public void setConsumacoes(List<ConsumacaoDTO> consumacoes) {
		this.consumacoes = consumacoes;
	}
	@Override
	public String toString() {
		return "CheckinResponseDTO [checkinId=" + checkinId + ", clientDTO=" + clientDTO + ", consumacoes="
				+ consumacoes + "]";
	}
	
}

package br.edu.infnet.order.Resource.dto;

public class ConsumacaoDTO {
	
	private Integer id;
	private int numero;
	private int valor;
	private String descricao;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public int getNumero() {
		return numero;
	}
	public void setNumero(int numero) {
		this.numero = numero;
	}
	public int getValor() {
		return valor;
	}
	public void setValor(int valor) {
		this.valor = valor;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	@Override
	public String toString() {
		return "ConsumacaoDTO [id=" + id + ", numero=" + numero + ", valor=" + valor + ", descricao=" + descricao + "]";
	}
	
	
}

package br.edu.infnet.order.Resource.dto;

import java.util.List;

public class CheckinDTO {
	
	private Integer clientID;
	private Integer checkinId;
	private List<ConsumacaoDTO> consumacoes;
	public Integer getClientID() {
		return clientID;
	}
	public void setClientID(Integer clientID) {
		this.clientID = clientID;
	}
	public Integer getCheckinId() {
		return checkinId;
	}
	public void setCheckinId(Integer checkinId) {
		this.checkinId = checkinId;
	}
	public List<ConsumacaoDTO> getConsumacoes() {
		return consumacoes;
	}
	public void setConsumacoes(List<ConsumacaoDTO> consumacoes) {
		this.consumacoes = consumacoes;
	}
	@Override
	public String toString() {
		return "CheckinDTO [clientID=" + clientID + ", checkinId=" + checkinId + ", consumacoes=" + consumacoes + "]";
	}
	
}

package br.edu.infnet.order.clients;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

import br.edu.infnet.order.Resource.dto.ConsumacaoDTO;

@FeignClient("consumacao")
public interface ConsumacaoClient {
	
	@RequestMapping("/consumacoes")
	public List<ConsumacaoDTO> getConsumacoes();
		
}

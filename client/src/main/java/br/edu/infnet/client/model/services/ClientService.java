package br.edu.infnet.client.model.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.edu.infnet.client.model.entities.Client;
import br.edu.infnet.client.model.repository.ClientRepository;

@Service
public class ClientService {
	
	@Autowired
	private ClientRepository clientRepository;
	
	public Client getByCodigo(Long codigo) {
		return clientRepository.findById(codigo).get();
	}

	public List<Client> getAll() {
	    List<Client> clients = new ArrayList<>();
	    clientRepository.findAll().forEach(clients::add);
	    return clients;
	}
	
	public Client save(Client client) {
	    return clientRepository.save(client);
	}
	
	public void deleteByCodigo(Long codigo) {
	    clientRepository.deleteById(codigo);
	}


}